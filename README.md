# GitLab-Runner for Slurm and Sarus container runtime

## Installing the runner
1. Clone the repo in say `~/gitlab-runner` and `cd ~/gitlab-runner`

2. Run `./bin/gitlab-runner register -c $PWD/etc/config.toml`. Enter the token from gitlab.

3. Copy some settings from `./etc/config.stub.toml` to `./etc/config.toml` if necessary.

4. Create `./etc/defaults.sh` which is loaded before execution to define default slurm variables, see `./etc/defaults-stub.sh` for an example.

## Launch the runner
```
./bin/gitlab-runner -c $PWD/etc/config.toml
```

## Configuring Slurm in CI jobs

In your `.gitlab-ci.yml` script, you can define variables to control how slurm handles your job:

```yaml
my job:
  image: ubuntu:18.04
  variables:
    SLURM_NTASKS: 10
  script: echo "hello world"
  tags:
    - my_runner
```

For the full list of environment variables, please check out

**https://slurm.schedmd.com/srun.html** section **INPUT ENVIRONMENT VARIABLES**.

## More job configuration variables

There are some variables to configure the container runtime as well:


| Variable                 | Values (default in bold)  | Description                                                                                                                                                            |
|--------------------------|---------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `PULL_IMAGE`             | `YES` / **`NO`**          | When set to `YES`, the image will be pulled from the registry                                                                                                          |
| `ALLOCATION_NAME`        | (string)                  | Name for the Slurm allocation. Useful when running multiple jobs without having to allocate resources each time. Typical value is `my-pipeline-$CI_PIPELINE_ID`.       |
| `ONLY_ALLOCATE`          | `1` / **`0`**             | Set to `1` to create a slurm allocation with the name `$ALLOCATION_NAME`. The `before_script`, `script` and `after_script` will not be executed.                       |
| `ONLY_DEALLOCATE`        | `1` / **`0`**             | Set to `1` to cancel a slurm allocation with the name `$ALLOCATION_NAME`. The `before_script`, `script` and `after_script` will not be executed.                       |
| `DISABLE_AFTER_SCRIPT`   | `YES` / **`NO`**          | Set to `YES` to disable the `after_script`. This removes the overhead of running an empty `after_script` command through slurm.                                        |
| `USE_MPI`                | `YES` / **`NO`**          | Set to `YES` to enable MPI communication between containers                                                                                                            |
| `CRAY_CUDA_MPS`          | (unset)                   | This variable must be set to make one GPU available to multiple containers.                                                                                            |
| `USE_AMDGPU`             | `YES` / **`NO`**          | Set to `YES` to enable AMD GPU support                                                                                                                                 |
| `SARUS_VERBOSE`          | `YES` / **`NO`**          | Use verbose output for the container runtime                                                                                                                           |
| `SARUS_VERSION`          | (string)                  | Specify the module version to load, e.g. `sarus/1.5.1`                                                                                                                 |
| `GIT_STRATEGY`           | **none** / fetch / clone  | Strategy to fetch sources. By default we do not fetch sources, our assumption is that the container has all relevant information                                       |
