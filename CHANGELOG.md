## Version v0.4.0
- CSCS_REGISTRY_USER, CSCS_REGISTRY_PASSWORD and CSCS_REGISTRY_LOGIN are now deprecated.
  Credentials  are provided by the ci-ext middleware and automatically fetched
## Version v0.3.5
- `SLURM_EXACT` is a whitelisted variable now.
## Version v0.3.3
- `VERBOSE` flag has been removed since it is too verbose w.r.t. secrets in bash
  variables. Instead we run `salloc`, `srun` and `scancel` with `set -x` to
  show what is actually being run.

## Version v0.3.2
- Fix Slurm variables and add --nodelist flag.
## Version v0.3.1
- Updated to gitlab-runner to 13.8.0
- Added a link to here in case people run into issues.
## Version v0.3.0

- `REGISTRY_USER` and `REGISTRY_PASSWORD` have been deprecated in favor of
  `CSCS_REGISTRY_USER` and `CSCS_REGISTRY_PASSWORD`.
- `CSCS_REGISTRY_LOGIN=YES` is now a required variable to make `sarus` use
  the login credentials when pulling from a private Docker registry.
