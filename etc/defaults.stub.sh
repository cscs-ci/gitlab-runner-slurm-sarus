#/usr/bin/env bash
export SLURM_JOB_NUM_NODES="1"
export SLURM_TIMELIMIT="15:00"
export SLURM_CONSTRAINT="mc"
export SLURM_PARTITION="cscsci"
export SLURM_NTASKS="1"
export SALLOC_ACCOUNT="djenkssl"
