#!/bin/bash

set -e
set -o pipefail

[[ ! -v CUSTOM_ENV_CSCS_CI_MW_URL ]] && CUSTOM_ENV_CSCS_CI_MW_URL="https://cicd-ext-mw.cscs.ch/ci"

function ensure_runner_mode_allowed {
    # check if we are allowed to execute the requested mode
    local ALLOWED_MODE=$(curl --retry 10 --retry-connrefused --silent "$CUSTOM_ENV_CSCS_CI_MW_URL/allowance/runner?token=$CUSTOM_ENV_CI_JOB_TOKEN&job_id=$CUSTOM_ENV_CI_JOB_ID&num_nodes=${CUSTOM_ENV_SLURM_JOB_NUM_NODES:-1}")
    local ALLOWED=$(echo "$ALLOWED_MODE" | jq --raw-output '.allowed' || echo "'$ALLOWED_MODE' is not a valid JSON string")
    MODE=$(echo "$ALLOWED_MODE" | jq --raw-output '.mode' || echo "invalid-mode")

    if [[ "$ALLOWED" != "YES" ]] ; then
        echo "Permission error. Reason: $ALLOWED" 1>&2
        exit $BUILD_FAILURE_EXIT_CODE
    fi
}

CURRENT_DIR=$(dirname $(realpath "${BASH_SOURCE[0]:-$0}"))
source "$CURRENT_DIR/../etc/defaults.sh"
source "$CURRENT_DIR/common.sh"

# If the want to run something, it requires an image
if ! truthy "$CUSTOM_ENV_ONLY_ALLOCATE" && ! truthy "$CUSTOM_ENV_ONLY_DEALLOCATE" && [[ -z "$CUSTOM_ENV_CI_JOB_IMAGE" ]]; then
    echo "Provide an image"
    exit "$BUILD_FAILURE_EXIT_CODE"
fi

if truthy "$CUSTOM_ENV_ONLY_ALLOCATE" && [[ -z "${CUSTOM_ENV_ALLOCATION_NAME}" ]]; then
    echo "Provide an allocation name when just allocating"
    exit "$BUILD_FAILURE_EXIT_CODE"
fi

if truthy "$CUSTOM_ENV_ONLY_DEALLOCATE" && -z "${CUSTOM_ENV_ALLOCATION_NAME}" ]]; then
    echo "Provide an allocation name when just deallocating"
    exit "$BUILD_FAILURE_EXIT_CODE"
fi

# There is nothing to prepare when deallocating!
if truthy "$CUSTOM_ENV_ONLY_DEALLOCATE" == "1" ; then
    exit 0
fi

echo -e "Using image ${b}${CUSTOM_ENV_CI_JOB_IMAGE}${x}"

# Allocate resources
source "$CURRENT_DIR/setup_slurm.sh" YES

# If a custom name is given, use that.
if [[ -z "$CUSTOM_ENV_ALLOCATION_NAME" ]]; then
    NAME="$CUSTOM_ENV_CI_COMMIT_SHORT_SHA-$CUSTOM_ENV_CI_JOB_ID"
else
    NAME="$CUSTOM_ENV_ALLOCATION_NAME"
fi

OLDJOBID=$(squeue -h --name="$NAME" --format=%A --states="RUNNING" | head -n1)

if [[ -z "$OLDJOBID" ]]; then
    ensure_runner_mode_allowed

    echo -e "Allocating a job with name ${b}$NAME${x}."
    NODELIST=""
    [[ -n "$SLURM_JOB_NODELIST" ]] && NODELIST="--nodelist=$SLURM_JOB_NODELIST"

    start_alloc=$(date +%s)

    ( set -x ; salloc \
        --no-shell \
        --time="$SLURM_TIMELIMIT" \
        --job-name="$NAME" \
        --constraint="$SLURM_CONSTRAINT" \
        --nodes="$SLURM_JOB_NUM_NODES" \
        --partition="$SLURM_PARTITION" \
        $NODELIST )

    if [ $? -ne 0 ]; then
        echo "Could not allocate resources"
        exit "$BUILD_FAILURE_EXIT_CODE"
    fi

    JOBID=$(squeue -h --name="$NAME" --format=%A --states="RUNNING" | head -n1)

    end_alloc=$(date +%s)
    let waiting_alloc=($end_alloc - $start_alloc)
    echo "Waited $waiting_alloc seconds for allocation"

    # send job meta data to middleware
    if [[ -z "$CLUSTER_NAME" && -f /etc/xthostname ]] ; then
        CLUSTER_NAME=$(cat /etc/xthostname)
    fi
    JOB_META_DATA='{"mode":"container-runner","allocation_wait_time":'$waiting_alloc',"slurm_job_id":"'$JOBID'","machine":"'$CLUSTER_NAME'","num_nodes":'$SLURM_JOB_NUM_NODES'}'
    curl -s --retry 5 --retry-connrefused -H "Content-Type: application/json" --data-raw "${JOB_META_DATA}" "${CUSTOM_ENV_CSCS_CI_MW_URL}/redis/job?token=${CUSTOM_ENV_CI_JOB_TOKEN}&job_id=${CUSTOM_ENV_CI_JOB_ID}"
else
    echo -e "Using existing allocation ${b}${OLDJOBID}${x} with name ${b}${NAME}${x}"
    JOBID="${OLDJOBID}"
fi

# Pull the image if necessary
if truthy "$CUSTOM_ENV_PULL_IMAGE" ; then
    # load module sarus if not already in PATH
    if ! which sarus > /dev/null 2>&1 ; then
        if [[ -n $(type -t module) ]] ; then
            module load ${CUSTOM_ENV_SARUS_VERSION:-sarus}
        fi
    fi
    if ! which sarus > /dev/null 2>&1 ; then
        echo "Could not find sarus..."
        exit "$BUILD_FAILURE_EXIT_CODE"
    fi

    if truthy "$PULL_ON_HOST_MACHINE" ; then
        SLURM_PREFIX=""
    else
        SLURM_PREFIX="srun --quiet -N 1 -n 1 --chdir=${SCRATCH} --jobid=${JOBID}"
    fi

    PULL_LOCK_DIR="$SCRATCH/.sarus/pull_locks"
    mkdir -p "$PULL_LOCK_DIR"
    LOCK_PREFIX="flock -x $PULL_LOCK_DIR/$(echo $CUSTOM_ENV_CI_JOB_IMAGE | sha256sum | cut -d' ' -f1)"

    # Only provide CSCS registry credentials when the image resides in the expected CSCS_REGISTRY_PATH
    # Also we do not use credentials when something like `image: docker.io/ubuntu:22.04` is used
    if [[ "$CUSTOM_ENV_CI_JOB_IMAGE" == "$CUSTOM_ENV_CSCS_REGISTRY"/* ]] ; then
        echo -e "${b}Using login credentials when pulling from container registry${x}"
        creds_json=$(curl --retry 5 --silent "$CUSTOM_ENV_CSCS_CI_MW_URL/credentials?token=$CUSTOM_ENV_CI_JOB_TOKEN&job_id=$CUSTOM_ENV_CI_JOB_ID&creds=container_registry")
        creds=$(echo ${creds_json} | jq --join-output '.container_registry.username + "\\n" + .container_registry.password')
        # retry pulling up to 5 times with random sleeps in between
        retry "echo -e '${creds}' | $SLURM_PREFIX $LOCK_PREFIX sarus pull --login '$CUSTOM_ENV_CI_JOB_IMAGE'" 5 random 3 15 'find $SCRATCH/.sarus/ -cmin +1 -name "metadata.json.lock" -delete'
    else
        if [[ -v CUSTOM_ENV_CUSTOM_REGISTRY_USERNAME ]] ; then
            echo -e "${b}Using login credentials provided in CUSTOM_REGISTRY_USERNAME/CUSTOM_REGISTRY_PASSWORD when pulling from container registry${x}"
            retry "echo -e '${CUSTOM_ENV_CUSTOM_REGISTRY_USERNAME}\n${CUSTOM_ENV_CUSTOM_REGISTRY_PASSWORD}' | $SLURM_PREFIX $LOCK_PREFIX sarus pull --login '$CUSTOM_ENV_CI_JOB_IMAGE'" 5 random 3 15 'find $SCRATCH/.sarus/ -cmin +1 -name "metadata.json.lock" -delete'
        else
            echo -e "${b}Pulling image anonymously${x}"
            retry "$SLURM_PREFIX $LOCK_PREFIX sarus pull '$CUSTOM_ENV_CI_JOB_IMAGE'" 5 random 3 15 'find $SCRATCH/.sarus/ -cmin +1 -name "metadata.json.lock" -delete'
        fi
    fi

    if [ $? -ne 0 ]; then
        echo -e "Could not load the image ${b}${CUSTOM_ENV_CI_JOB_IMAGE}${x}"
        exit "$BUILD_FAILURE_EXIT_CODE"
    fi
fi

exit 0
