#!/bin/bash

CURRENT_DIR=$(dirname $(realpath "${BASH_SOURCE[0]:-$0}"))
source "$CURRENT_DIR/../etc/defaults.sh"
source "$CURRENT_DIR/common.sh"

# In case we are here to deallocate only.
if truthy "$CUSTOM_ENV_ONLY_DEALLOCATE" ; then
    NAME="$CUSTOM_ENV_ALLOCATION_NAME"
    JOBID=$(squeue -h --name="$NAME" --format=%A --states="RUNNING" | head -n1)

    echo -e "Deallocating resources of ${b}${JOBID}${x} with name ${b}${NAME}${x}"

    if [[ -n "$JOBID" ]]; then
        ( set -x ; scancel "$JOBID" )
    fi

    # Try to remove the image as well
    # For now disabled, since it's easier when using multiple child pipelines with the same image
    # TODO: decouple removing images from deallocating resources?
    # module load sarus
    # sarus rmi "$CUSTOM_ENV_CI_JOB_IMAGE"

    exit 0
fi

# In case we were here to allocate only
if truthy "$CUSTOM_ENV_ONLY_ALLOCATE" ; then
    exit 0
fi

# Otherwise, we are running a job, and we have to check whether we
# own the allocation, or if we are using an external allocation.
# Only when we *own* the allocation (i.e. it was created inside of
# this job), we deallocate it.

# In case we own the allocation (no custom allocation provided),
# we will clean up our resources after us.
if [[ -z "$CUSTOM_ENV_ALLOCATION_NAME" ]]; then
    NAME="$CUSTOM_ENV_CI_COMMIT_SHORT_SHA-$CUSTOM_ENV_CI_JOB_ID"
    JOBID=$(squeue -h --name="$NAME" --format=%A --states="RUNNING" | head -n1)

    echo -e "Deallocating resources of ${b}${JOBID}${x} with name ${b}${NAME}${x}"

    if [[ -n "$JOBID" ]]; then
        ( set -x ; scancel "$JOBID" )
    fi
fi

if [[ ! -v CUSTOM_ENV_GIT_STRATEGY || "$CUSTOM_ENV_GIT_STRATEGY" == "none" ]] ; then
    rm -Rf "$CUSTOM_ENV_CI_PROJECT_DIR"
elif [[ ${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID} -gt 1 ]] ; then
    # only keep at most 2 slots with cloned sources
    rm -Rf "$CUSTOM_ENV_CI_PROJECT_DIR"
fi

# cleanup cache folders older than 720 minutes
find "$SCRATCH/gitlab-runner/cache/${CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN}/" -mindepth 3 -maxdepth 3 -type d -cmin +720 -exec rm -Rf {} +

exit 0
