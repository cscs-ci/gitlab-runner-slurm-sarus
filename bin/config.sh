#!/usr/bin/env bash

# exit when we run as root - this is not what we want
[[ $(id -u) == 0 ]] && echo "Running as root user" 1>&2 && exit $BUILD_FAILURE_EXIT_CODE
[[ ! -v SCRATCH ]] && echo "SCRATCH variable is empty. This is an error" 1>&2 && exit $BUILD_FAILURE_EXIT_CODE

cat << EOS
{
  "builds_dir": "${SCRATCH}/gitlab-runner/builds/${CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN}/${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID}",
  "cache_dir": "${SCRATCH}/gitlab-runner/cache/${CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN}/${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID}/${CUSTOM_ENV_CI_PROJECT_PATH_SLUG}",
  "builds_dir_is_shared": false,
  "driver": {
    "name": "Sarus runner [ https://gitlab.com/cscs-ci/gitlab-runner-slurm-sarus/ ]",
    "version": "v0.4.0"
  }
}
EOS
