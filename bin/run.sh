#!/usr/bin/env bash

set -e
set -o pipefail

CURRENT_DIR=$(dirname $(realpath "${BASH_SOURCE[0]:-$0}"))
source "$CURRENT_DIR/../etc/defaults.sh"
source "$CURRENT_DIR/common.sh"

file="$1"
stage="$2"

[[ ! -x "$file" ]] && echo "File $file is not executable" && exit $BUILD_FAILURE_EXIT_CODE

# Add an option to disable the after script because slurm is so slow...
if [[ "${stage}" == "after_script" ]] && truthy "${CUSTOM_ENV_DISABLE_AFTER_SCRIPT}" ; then
    exit 0
fi

if [[ "${stage}" == "build_script" ]] || [[ "${stage}" == "after_script" ]]; then
    # Create a shared folder which can be used within a single pipeline across multiple jobs
    CI_CACHE_FOLDER="${SCRATCH}/gitlab-runner/cache/${CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN}/${CUSTOM_ENV_CI_PROJECT_PATH_SLUG}/${CUSTOM_ENV_CI_PIPELINE_ID}"

    mkdir -p "${CI_CACHE_FOLDER}"

    # Do not execute anything when just allocating.
    if truthy "$CUSTOM_ENV_ONLY_ALLOCATE" || truthy "$CUSTOM_ENV_ONLY_DEALLOCATE" ; then
        exit 0
    fi

    # load module sarus if not already in PATH
    if ! which sarus > /dev/null 2>&1 ; then
        module load ${CUSTOM_ENV_SARUS_VERSION:-sarus}
    fi

    source "$CURRENT_DIR/setup_slurm.sh"

    # Reuse the named allocation if given, otherwise the default.
    if [[ -z "$CUSTOM_ENV_ALLOCATION_NAME" ]]; then
        NAME="$CUSTOM_ENV_CI_COMMIT_SHORT_SHA-$CUSTOM_ENV_CI_JOB_ID"
    else
        NAME="$CUSTOM_ENV_ALLOCATION_NAME"
    fi

    if truthy "$CUSTOM_ENV_USE_MPI" ; then
        MPI_FLAG="--mpi"
    else
        MPI_FLAG=""
    fi

    if truthy "$CUSTOM_ENV_USE_AMDGPU" ; then
        AMDGPU_FLAG="--device=/dev/dri --device=/dev/kfd"
    else
        AMDGPU_FLAG=""
    fi

    if truthy "$CUSTOM_ENV_SARUS_VERBOSE" ; then
        VERBOSE_FLAG="--verbose"
    else
        VERBOSE_FLAG=""
    fi

    if truthy "$CUSTOM_ENV_TIME" ; then
        TIME="time"
    else
        TIME=""
    fi

    # Find the corresponding job id
    JOBID=$(squeue -h --name="$NAME" --format=%A --states="RUNNING" | head -n1)

    ADDITIONAL_MOUNTS=()
    for dataname in $CUSTOM_ENV_CSCS_NEEDED_DATA ; do
        if [[ -f "$CURRENT_DIR"/../etc/known_mounts/$dataname ]] ; then
            MOUNT_DIR="$(cat "$CURRENT_DIR"/../etc/known_mounts/$dataname)"
            ADDITIONAL_MOUNTS+=("--mount=type=bind,src=$MOUNT_DIR,dst=$MOUNT_DIR")
        fi
    done

    # Move the gitlab ci script into a folder that we can mount
    SCRIPT_NAME=$(basename "$1")_$(uuidgen)
    mkdir -p "${CUSTOM_ENV_CI_PROJECT_DIR}"
    cp "$1" "${CUSTOM_ENV_CI_PROJECT_DIR}/$SCRIPT_NAME"

    # sarus log
    SARUS_LOG="${CUSTOM_ENV_CI_PROJECT_DIR}"/sarus.log
    rm -f "$SARUS_LOG"
    touch "$SARUS_LOG"
    tail -F "$SARUS_LOG" &
    TAIL_PID=$!

    CLEANENV+=("CI_CACHE_FOLDER=$CI_CACHE_FOLDER")

    # Run ci script through sarus with a clean environment
    if ! "${CLEANENV[@]}" bash -c "set -x ; srun \
        --chdir='${CUSTOM_ENV_CI_PROJECT_DIR}' \
        --jobid='${JOBID}' \
        $TIME \
        sarus \
            $VERBOSE_FLAG \
            run \
            $MPI_FLAG \
            $AMDGPU_FLAG \
            --mount=type=bind,source='${CUSTOM_ENV_CI_PROJECT_DIR}',destination='${CUSTOM_ENV_CI_PROJECT_DIR}' \
            --mount=type=bind,source='${CI_CACHE_FOLDER}',destination='${CI_CACHE_FOLDER}' \
            --annotation "com.hooks.logging.stderrfd=$SARUS_LOG" \
            --annotation "com.hooks.logging.stdoutfd=$SARUS_LOG" \
            "${ADDITIONAL_MOUNTS[@]}" \
            '${CUSTOM_ENV_CI_JOB_IMAGE}' \
            '${CUSTOM_ENV_CI_PROJECT_DIR}/$SCRIPT_NAME'"
    then
        kill $TAIL_PID
        exit "$BUILD_FAILURE_EXIT_CODE"
    fi
    kill $TAIL_PID
elif [[ "${stage}" == "get_sources" ]]; then
    if [[ -v CUSTOM_ENV_GIT_STRATEGY ]] ; then
        # retry getting sources up to 3 times, deleting the whole folder in between retries
        retry "$1" 3 random 10 30 "rm -Rf '$CUSTOM_ENV_CI_PROJECT_DIR'"
    else
        echo "This runner does not fetch the source code by default. If you need the source code, please set the variable GIT_STRATEGY to either clone or fetch."
        rm -Rf "$CUSTOM_ENV_CI_PROJECT_DIR"
        mkdir -p "$CUSTOM_ENV_CI_PROJECT_DIR"
    fi
else
  ${1}
fi

exit 0
